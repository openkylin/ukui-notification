/*
 * Copyright (C) 2023, KylinSoft Co., Ltd.
 *
 *  This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include "notice-menu.h"
#include <QDebug>
#include <QVBoxLayout>
#include <ukcc/widgets/fixlabel.h>

NoticeMenu::NoticeMenu(QWidget *parent) :
    QMenu(parent)
{
    setFixedSize(380, 190);
    initUi();
    setConnect();
}

void NoticeMenu::initStatus(UkuiNotification::SingleApplicationSettings *settings)
{
    m_voiceBtn->blockSignals(true);
    m_showBtn->blockSignals(true);
    m_detailBtn->blockSignals(true);
//    m_styleGroup->blockSignals(true);

    m_voiceBtn->setChecked(settings->allowSound());
    m_detailBtn->setChecked(settings->showContentOnLockScreen());
    m_showBtn->setChecked(settings->showNotificationOnLockScreen());

//    int styleId = 0;
//    UkuiNotification::SettingsProperty::Property style = settings->popupStyle();
//    if (style == UkuiNotification::SettingsProperty::TransientPopup) {
//        styleId = 0;
//    } else if (style == UkuiNotification::SettingsProperty::ResidentPopup) {
//        styleId = 1;
//    } else if (style == UkuiNotification::SettingsProperty::NoPopup) {
//        styleId = 2;
//    }
//    m_styleGroup->button(styleId)->setChecked(true);

    m_voiceBtn->blockSignals(false);
    m_showBtn->blockSignals(false);
    m_detailBtn->blockSignals(false);
//    m_styleGroup->blockSignals(false);
}

void NoticeMenu::setVoiceEnable(bool state)
{
    m_voiceBtn->blockSignals(true);
    m_voiceBtn->setChecked(state);
    m_voiceBtn->blockSignals(false);
}

void NoticeMenu::setDetailShowOnLockScreenEnable(bool state)
{
    m_detailBtn->blockSignals(true);
    m_detailBtn->setChecked(state);
    m_detailBtn->blockSignals(false);
}

void NoticeMenu::setShowOnLockScreenEnable(bool state)
{
    m_showBtn->blockSignals(true);
    m_showBtn->setChecked(state);
    m_showBtn->blockSignals(false);
}

void NoticeMenu::setPopupStyle(UkuiNotification::SettingsProperty::Property style)
{
    Q_UNUSED(style)
//    int styleId = 0;
//    if (style == UkuiNotification::SettingsProperty::TransientPopup) {
//        styleId = 0;
//    } else if (style == UkuiNotification::SettingsProperty::ResidentPopup) {
//        styleId = 1;
//    } else if (style == UkuiNotification::SettingsProperty::NoPopup) {
//        styleId = 2;
//    }
//    m_styleGroup->blockSignals(true);
//    m_styleGroup->button(styleId)->setChecked(true);
//    m_styleGroup->blockSignals(false);
}

void NoticeMenu::initUi()
{
    QVBoxLayout *menuLyt = new QVBoxLayout(this);
    menuLyt->setSpacing(0);
    menuLyt->setContentsMargins(8, 0, 8, 0);

    QFrame *voiceFrame = new QFrame(this);
    m_voiceBtn = new KSwitchButton;
    setFrame(voiceFrame, tr("Beep sound when notified"), m_voiceBtn);

    QFrame *line_1 = setLine();

    QFrame *detailFrame = new QFrame(this);
    m_detailBtn = new KSwitchButton;
    setFrame(detailFrame, tr("Show message  on screenlock"), m_detailBtn);

    QFrame *line_2 = setLine();

    QFrame *showFrame = new QFrame(this);
    m_showBtn = new KSwitchButton;
    setFrame(showFrame, tr("Show noticfication  on screenlock"), m_showBtn);

//    QFrame *line_3 = setLine();

//    QLabel *styletitleLabel = new QLabel(tr("Notification Style"), this);
//    styletitleLabel->setContentsMargins(24, 0, 0, 0);
//
//    m_styleGroup = new QButtonGroup(this);
//
//    QFrame *mutativeFrame = new QFrame(this);
//    m_mutativeBtn = new QRadioButton;
//    m_styleGroup->addButton(m_mutativeBtn, 0);
//    setFrame(mutativeFrame, tr("Banner: Appears in the upper right corner of the screen, and disappears automatically"), m_mutativeBtn);
//
//    QFrame *alwaysFrame = new QFrame(this);
//    m_alwaysBtn = new QRadioButton;
//    m_styleGroup->addButton(m_alwaysBtn, 1);
//    setFrame(alwaysFrame, tr("Tip:It will be kept on the screen until it is closed"), m_alwaysBtn);
//
//    QFrame *noneFrame = new QFrame(this);
//    m_noneBtn = new QRadioButton;
//    m_styleGroup->addButton(m_noneBtn, 2);
//    setFrame(noneFrame, tr("None:Notifications will not be displayed on the screen, but will go to the notification center"), m_noneBtn);

    menuLyt->addWidget(voiceFrame);
    menuLyt->addWidget(line_1);
    menuLyt->addWidget(detailFrame);
    menuLyt->addWidget(line_2);
    menuLyt->addWidget(showFrame);
//    menuLyt->addWidget(line_3);
    menuLyt->addSpacing(16);
//    menuLyt->addWidget(styletitleLabel);
//    menuLyt->addWidget(mutativeFrame);
//    menuLyt->addWidget(alwaysFrame);
//    menuLyt->addWidget(noneFrame);
    menuLyt->addStretch();

    // 隐藏未实现模块
//    voiceFrame->hide();
//    line_1->hide();
//    detailFrame->hide();
//    line_2->hide();
//    line_3->hide();
//    styletitleLabel->hide();
//    mutativeFrame->hide();
//    alwaysFrame->hide();
//    noneFrame->hide();
//    setFixedHeight(60);

}

void NoticeMenu::setFrame(QFrame *frame, QString str, KSwitchButton *btn)
{
    frame->setFixedHeight(60);
    frame->setFrameShape(QFrame::NoFrame);
    QHBoxLayout *hLyt = new QHBoxLayout(frame);
    hLyt->setContentsMargins(24, 0, 24, 0);
    hLyt->setSpacing(8);
    FixLabel *label = new FixLabel(str, frame);
    label->setFixedWidth(250);
    hLyt->addWidget(label);
    hLyt->addStretch();
    hLyt->addWidget(btn);
}

void NoticeMenu::setFrame(QFrame *frame, QString str, QRadioButton *btn)
{
    frame->setFixedHeight(48);
    frame->setFrameShape(QFrame::NoFrame);
    QHBoxLayout *hLyt = new QHBoxLayout(frame);
    hLyt->setContentsMargins(24, 0, 24, 0);
    FixLabel *label = new FixLabel(str, frame);
    label->setFixedWidth(288);
    hLyt->addWidget(btn);
    hLyt->addWidget(label);
    hLyt->addStretch();
}

QFrame *NoticeMenu::setLine()
{
    QFrame *line = new QFrame();
    line->setMinimumSize(QSize(0, 1));
    line->setMaximumSize(QSize(16777215, 1));
    line->setLineWidth(0);
    line->setFrameShape(QFrame::HLine);
    line->setFrameShadow(QFrame::Sunken);
    return line;
}

void NoticeMenu::setConnect()
{
    connect(m_voiceBtn, &KSwitchButton::stateChanged, this, &NoticeMenu::voiceSignals);
    connect(m_detailBtn, &KSwitchButton::stateChanged, this, &NoticeMenu::detailSignals);
    connect(m_showBtn, &KSwitchButton::stateChanged, this, &NoticeMenu::showSignals);
//    connect(m_styleGroup, QOverload<int>::of(&QButtonGroup::buttonClicked), this, &NoticeMenu::styleBtnSignals);
}

void NoticeMenu::showEvent(QShowEvent *event)
{
    int menuXPos = this->pos().x();
    int menuWidth = this->size().width()-4;
    int buttonWidth = 36;
    QPoint pos = QPoint(menuXPos - menuWidth + buttonWidth,
                        this->pos().y());
    this->move(pos);
    QMenu::showEvent(event);
}
