<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ar" sourcelanguage="en">
<context>
    <name>Notice</name>
    <message>
        <location filename="../notification-ukcc-plugin.cpp" line="52"/>
        <source>Notice</source>
        <translation>لاحظ</translation>
    </message>
    <message>
        <source>NotFaze Mode</source>
        <translation type="vanished">勿扰模式</translation>
    </message>
    <message>
        <location filename="../notification-ukcc-plugin.cpp" line="149"/>
        <source>Do not disturb mode</source>
        <translation>وضع عدم الإزعاج</translation>
        <extra-contents_path>/Notice/Do not disturb mode Mode</extra-contents_path>
    </message>
    <message>
        <location filename="../notification-ukcc-plugin.cpp" line="151"/>
        <source>(Notification banners, prompts will be hidden, and notification sounds will be muted)</source>
        <translation>(سيتم إخفاء لافتات الإشعارات والمطالبات وسيتم كتم أصوات الإشعارات)</translation>
    </message>
    <message>
        <location filename="../notification-ukcc-plugin.cpp" line="226"/>
        <source>Automatically turn on</source>
        <translation>التفعيل التلقائي</translation>
        <extra-contents_path>/Notice/Do not disturb mode Mode/Automatically turn on</extra-contents_path>
    </message>
    <message>
        <location filename="../notification-ukcc-plugin.cpp" line="251"/>
        <source>to</source>
        <translation>ل</translation>
    </message>
    <message>
        <location filename="../notification-ukcc-plugin.cpp" line="277"/>
        <source>Automatically turn on when multiple screens are connected</source>
        <translation>التشغيل التلقائي عند توصيل شاشات متعددة</translation>
        <extra-contents_path>/Notice/Do not disturb mode/Automatically turn on when multiple screens are connected</extra-contents_path>
    </message>
    <message>
        <location filename="../notification-ukcc-plugin.cpp" line="283"/>
        <source>Automatically open in full screen mode</source>
        <translation>فتح تلقائيا في وضع ملء الشاشة</translation>
        <extra-contents_path>/Notice/Do not disturb mode/Automatically open in full screen mode</extra-contents_path>
    </message>
    <message>
        <location filename="../notification-ukcc-plugin.cpp" line="289"/>
        <source>Allow automatic alarm reminders in Do Not Disturb mode</source>
        <translation>السماح بتذكيرات التنبيه التلقائي في وضع عدم الإزعاج</translation>
        <extra-contents_path>/Notice/Do not disturb mode/Allow automatic alarm reminders in Do Not Disturb mode</extra-contents_path>
    </message>
    <message>
        <location filename="../notification-ukcc-plugin.cpp" line="318"/>
        <source>Notice Settings</source>
        <translation>إعدادات الإشعار</translation>
    </message>
    <message>
        <location filename="../notification-ukcc-plugin.cpp" line="320"/>
        <source>Get notifications from the app</source>
        <translation>الحصول على إشعارات من التطبيق</translation>
        <extra-contents_path>/notice/Get notifications from the app</extra-contents_path>
    </message>
</context>
<context>
    <name>NoticeMenu</name>
    <message>
        <location filename="../notice-menu.cpp" line="104"/>
        <source>Beep sound when notified</source>
        <translation>صوت صفير عند الإخطار</translation>
    </message>
    <message>
        <location filename="../notice-menu.cpp" line="110"/>
        <source>Show message  on screenlock</source>
        <translation>إظهار الرسالة على قفل الشاشة</translation>
    </message>
    <message>
        <location filename="../notice-menu.cpp" line="116"/>
        <source>Show noticfication  on screenlock</source>
        <translation>إظهار الإشعارات على قفل الشاشة</translation>
    </message>
    <message>
        <source>Notification Style</source>
        <translation type="vanished">通知样式</translation>
    </message>
    <message>
        <source>Banner: Appears in the upper right corner of the screen, and disappears automatically</source>
        <translation type="vanished">横幅:显示在屏幕右上角，会自动消失</translation>
    </message>
    <message>
        <source>Tip:It will be kept on the screen until it is closed</source>
        <translation type="vanished">提示:会保留在屏幕上，直到被关闭</translation>
    </message>
    <message>
        <source>None:Notifications will not be displayed on the screen, but will go to the notification center</source>
        <translation type="vanished">无:通知不会显示在屏幕上，但会进入通知中心</translation>
    </message>
</context>
</TS>
