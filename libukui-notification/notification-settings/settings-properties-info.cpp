/*
 * Copyright (C) 2023, KylinSoft Co., Ltd.
 *
 *  This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: iaom <zhangpengfei@kylinos.cn>
 */

#include "settings-properties-info.h"
#include <QLocale>
#include <QTime>

using namespace UkuiNotification;

class UkuiNotification::SettingsPropertiesInfoPrivate
{
public:
    SettingsProperty::Property m_property;
    QString m_name;
    QString m_displayName;
    QMetaType::Type m_valueType;
    QString m_defaultValue;
    QMetaType::Type m_settingsPropertyMetaType;
};

SettingsPropertiesInfo::SettingsPropertiesInfo(): d(new SettingsPropertiesInfoPrivate)
{
    d->m_property = SettingsProperty::Invalid;
    d->m_name = QStringLiteral("Invalid");
    d->m_valueType = QMetaType::UnknownType;
    d->m_settingsPropertyMetaType = static_cast<QMetaType::Type>(QMetaType::type("UkuiNotification::SettingsProperty::Property"));
}

SettingsPropertiesInfo::SettingsPropertiesInfo(SettingsProperty::Property property)
    : d(new SettingsPropertiesInfoPrivate)
{
    d->m_property = property;

    switch (property) {
        case SettingsProperty::ScheduleTurnOnDND:
            d->m_name = QStringLiteral("ScheduleTurnOnDND");
            d->m_displayName = tr("Schedule turn on");
            d->m_valueType = QMetaType::Bool;
            d->m_defaultValue = "false";
            break;

        case SettingsProperty::ScheduleTurnOnDNDTime:
            d->m_name = QStringLiteral("ScheduleTurnOnDNDTime");
            d->m_displayName = tr("Schedule turn on time");
            d->m_valueType = QMetaType::QTime;
            d->m_defaultValue = "00:00";
            break;

        case SettingsProperty::ScheduleTurnOffDNDTime:
            d->m_name = QStringLiteral("ScheduleTurnOffDNDTime");
            d->m_displayName = tr("Schedule turn off time");
            d->m_valueType = QMetaType::QTime;
            d->m_defaultValue = "00:00";
            break;

        case SettingsProperty::DNDWhileMultiScreen:
            d->m_name = QStringLiteral("DNDWhileMultiScreen");
            d->m_displayName = tr("Turn on while multi-screen connected");
            d->m_valueType = QMetaType::Bool;
            d->m_defaultValue = "false";
            break;

        case SettingsProperty::DNDWhileFullScreen:
            d->m_name = QStringLiteral("DNDWhileFullScreen");
            d->m_displayName = tr("Turn on in full screen mode");
            d->m_valueType = QMetaType::Bool;
            d->m_defaultValue = "false";
            break;

        case SettingsProperty::NotifyAlarmWhileDND:
            d->m_name = QStringLiteral("NotifyAlarmWhileDND");
            d->m_displayName = tr("Allow alarm to notify in do not disturb mode");
            d->m_valueType = QMetaType::Bool;
            d->m_defaultValue = "false";
            break;

        case SettingsProperty::ReceiveNotificationsFromApps:
            d->m_name = QStringLiteral("ReceiveNotificationsFromApps");
            d->m_displayName = tr("Get notifications from applications");
            d->m_valueType = QMetaType::Bool;
            d->m_defaultValue = "true";
            break;

        case SettingsProperty::AllowNotify:
            d->m_name = QStringLiteral("AllowNotify");
            d->m_displayName = tr("Allow notify");
            d->m_valueType = QMetaType::Bool;
            d->m_defaultValue = "true";
            break;

        case SettingsProperty::AllowSound:
            d->m_name = QStringLiteral("AllowSound");
            d->m_displayName = tr("Allow sound for notifications");
            d->m_valueType = QMetaType::Bool;
            d->m_defaultValue = "true";
            break;

        case SettingsProperty::ShowContentOnLockScreen:
            d->m_name = QStringLiteral("ShowContentOnLockScreen");
            d->m_displayName = tr("Show notification content on lock screen");
            d->m_valueType = QMetaType::Bool;
            d->m_defaultValue = "false";
            break;

        case SettingsProperty::ShowNotificationOnLockScreen:
            d->m_name = QStringLiteral("ShowNotificationOnLockScreen");
            d->m_displayName = tr("Show notifications on lock screen");
            d->m_valueType = QMetaType::Bool;
            d->m_defaultValue = "false";
            break;

//        case SettingsProperty::PopupStyle:
//            d->m_name = QStringLiteral("popupStyle");
//            d->m_displayName = tr("Style of popup notification");
//            d->m_valueType = d->m_settingsPropertyMetaType;
//            d->m_defaultValue = "TransientPopup";
//            break;
//
//        case SettingsProperty::TransientPopup:
//            d->m_name = QStringLiteral("TransientPopup");
//            d->m_displayName = tr("Popup: displayed in the upper right corner of the screen, will automatically disappear");
//            d->m_valueType = QMetaType::Int;
//            break;
//
//        case SettingsProperty::ResidentPopup:
//            d->m_name = QStringLiteral("ResidentPopup");
//            d->m_displayName = tr("Notice: will remain on the screen until it is closed");
//            d->m_valueType = QMetaType::Int;
//            break;
//
//        case SettingsProperty::NoPopup:
//            d->m_name = QStringLiteral("NoPopup");
//            d->m_displayName = tr("None: notifications will not be displayed on the screen, only be put into notification center");
//            d->m_valueType = QMetaType::Int;
//            break;

        case SettingsProperty::Invalid:
            d->m_name = QStringLiteral("Invalid");
            d->m_valueType = QMetaType::UnknownType;
            break;
        // NOTE: new properties must also be added to ::fromName()
    }
}

SettingsPropertiesInfo::SettingsPropertiesInfo(const SettingsPropertiesInfo& pi)
    : d(new SettingsPropertiesInfoPrivate(*pi.d))
{
}

SettingsPropertiesInfo::~SettingsPropertiesInfo() = default;

SettingsPropertiesInfo& SettingsPropertiesInfo::operator=(const SettingsPropertiesInfo& rhs)
{
    *d = *rhs.d;
    return *this;
}

bool SettingsPropertiesInfo::operator==(const SettingsPropertiesInfo& rhs) const
{
    return d->m_name == rhs.d->m_name && d->m_displayName == rhs.d->m_displayName &&
           d->m_property == rhs.d->m_property && d->m_valueType == rhs.d->m_valueType;
}

QString SettingsPropertiesInfo::displayName() const
{
    return d->m_displayName;
}

QString SettingsPropertiesInfo::name() const
{
    return d->m_name;
}

SettingsProperty::Property SettingsPropertiesInfo::property() const
{
    return d->m_property;
}

QMetaType::Type SettingsPropertiesInfo::valueType() const
{
    return d->m_valueType;
}

SettingsPropertiesInfo SettingsPropertiesInfo::fromName(const QString& name)
{
    static QHash<QString, SettingsProperty::Property> propertyHash = {
        { QStringLiteral("ScheduleTurnOnDND"), SettingsProperty::ScheduleTurnOnDND },
        { QStringLiteral("ScheduleTurnOnDNDTime"), SettingsProperty::ScheduleTurnOnDNDTime },
        { QStringLiteral("ScheduleTurnOffDNDTime"), SettingsProperty::ScheduleTurnOffDNDTime },
        { QStringLiteral("DNDWhileMultiScreen"), SettingsProperty::DNDWhileMultiScreen },
        { QStringLiteral("DNDWhileFullScreen"), SettingsProperty::DNDWhileFullScreen },
        { QStringLiteral("NotifyAlarmWhileDND"), SettingsProperty::NotifyAlarmWhileDND },
        { QStringLiteral("ReceiveNotificationsFromApps"), SettingsProperty::ReceiveNotificationsFromApps },
        { QStringLiteral("AllowNotify"), SettingsProperty::AllowNotify },
        { QStringLiteral("AllowSound"), SettingsProperty::AllowSound },
        { QStringLiteral("ShowContentOnLockScreen"), SettingsProperty::ShowContentOnLockScreen },
        { QStringLiteral("ShowNotificationOnLockScreen"), SettingsProperty::ShowNotificationOnLockScreen },
//        { QStringLiteral("popupStyle"), SettingsProperty::PopupStyle },
//        { QStringLiteral("TransientPopup"), SettingsProperty::TransientPopup },
//        { QStringLiteral("ResidentPopup"), SettingsProperty::ResidentPopup },
//        { QStringLiteral("NoPopup"), SettingsProperty::NoPopup }
    };

    return {propertyHash.value(name)};
}

QString SettingsPropertiesInfo::defaultValue() const
{
    return d->m_defaultValue;
}

QString SettingsPropertiesInfo::valueToString(const QVariant &value) const
{
    if(d->m_valueType == d->m_settingsPropertyMetaType) {
        return SettingsPropertiesInfo(static_cast<SettingsProperty::Property>(value.toInt())).name();
    } else if(d->m_valueType == QMetaType::QTime) {
        return value.toTime().toString("HHmm");
    } else {
        return value.toString();
    }
}
