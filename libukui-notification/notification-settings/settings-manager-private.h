/*
 * Copyright (C) 2023, KylinSoft Co., Ltd.
 *
 *  This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: iaom <zhangpengfei@kylinos.cn>
 */

#ifndef NotificationSettingsPrivate_H
#define NotificationSettingsPrivate_H

#include "settings-manager.h"
#include <application-info.h>
#include <QJsonObject>
#include <QFileSystemWatcher>
#include <QList>
#include "settings-properties.h"

namespace UkuiNotification {


#define NOTIFICATION_SETTINGS_VERSION "1.0"

class SettingsManagerPrivate : public  QObject
{
    friend class SettingsManager;
    Q_OBJECT
public:
    explicit SettingsManagerPrivate(QObject* parent = nullptr);
    ~SettingsManagerPrivate();
    void init();

private Q_SLOTS:
    /**
     * @brief checkAndLoad
     * 检查通知配置文件，不存在则初始化生成默认配置文件，存在则根据配置文件版本判断是否更新配置文件。
     */
    void checkAndLoad();
private:
    /**
     * @brief createSettingsFile
     * 初始化创建通知配置文件。
     */
    void createSettingsFile();

    /**
     * @brief updateSettingsFile
     * 更新通知配置文件。
     */
    void updateSettingsFile();

    /**
     * @brief getAppInfo
     * 从搜索应用数据库服务中获取desktop信息。
     */
    QStringList getAppInfo();

    void initSettingsData(QJsonObject &data);
    void checkApplicationList(QJsonObject &data);

public Q_SLOTS:
    void desktopFileAdd(const QStringList &data);
    void desktopFileDelete(const QStringList &data);
Q_SIGNALS:
    void settingsUpdateFinished();
    void appUninstalled(const QString &desktopEntry);

private:
    void save2SettingsFile(const QJsonObject &jsonDocData);
    QJsonObject m_settingsData;
    QFileSystemWatcher *m_watcher = nullptr;
    UkuiSearch::ApplicationInfo * m_applicationInfo = nullptr;
    QJsonObject m_applicationDefaultSettings;
    bool m_needCheckApplist = true;
};

}
#endif // NotificationSettingsPrivate_H
