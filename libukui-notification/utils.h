/*
 * Copyright (C) 2023, KylinSoft Co., Ltd.
 *
 *  This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: iaom <zhangpengfei@kylinos.cn>
 */

#ifndef UKUI_NOTIFICATION_UTILS_H
#define UKUI_NOTIFICATION_UTILS_H
#include <QString>
namespace UkuiNotification {
/**
 * @short 提供一些方便的接口.
 * @author iaom
 */
namespace Utils {
    /**
     * @brief 根据进程号@p pid 获取应用的desktop文件。
     * @return desktop文件全路径
     */
    QString desktopEntryFromPid(uint pid);
    /**
     * @brief 根据进程号@p pid 获取应用所在的DISPLAY。
     * @return DISPLAY的值
     */
    QString displayFromPid(uint pid);
    /**
     * @brief 根据desktop文件名（不带后缀）获取全路径。
     * @return desktop文件全路径
     */
    QString desktopEntryFromName(const QString& desktopFileName);
}
}
#endif //UKUI_NOTIFICATION_UTILS_H
