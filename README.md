# ukui-notification

# 介绍
ukui-notificaton包含UKUI桌面环境的通知服务（ukui-notification-server）和通知开发接口（libukui-notifiaction）。
## 通知服务
ukui-notifiaction-server提供了遵循freedesktop[通知协议](https://specifications.freedesktop.org/notification-spec/notification-spec-latest.html)的通知服务接口。
目前支持的Capabilities：

actions;

body;

body-hyperlinks;

persistence;

sound.
### 扩展hints
目前ukui-notification-server的通知接口支持的扩展hints:

1.x-ukui-display 用于标识通知弹窗应该被显示在哪个DISPLAY上。

### 扩展接口
除协议规定的接口外，通知服务提供的额外接口：

1.org.ukui.NotificationServer.UpdateUnreadMessagesNumber (String desktop_entry, UInt32 number) ↦ ()

  用于发送应用当前未读消息数量

## 通知开发接口
#### 使用cmake:
```cmake
find_package(ukui-notification)
target_link_libraries(yourapp ukui-notification)
```
#### 使用qmake:
```qmake
CONFIG += link_pkgconfig
PKGCONFIG += ukui-notification
```
#### 关键接口：
notification-client.h 通知中心客户端注册、消息同步与action执行等功能。

popup-notification.h 单条通知的所有信息

notification-global-settings.h 全局通知设置

applications-settings.h 应用通知行为设置

single-application-settings.h 单个应用的通知设置
#### 代码编译：
mkdir build

cd build

cmake ..

make